//
//  iDockApp.swift
//  iDock
//
//  Created by Jordi Puigdellívol on 13/11/2020.
//

import SwiftUI

@main
struct iDockApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
