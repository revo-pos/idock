//
//  ETPPiDockControl.h
//  FrameworkTestApp
//
//  Created by Rey Jenald Peña on 2/16/15.
//  Copyright (c) 2015 Elo Touch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ETPPKeyInjectionAPICall.h"
#import "ETPPCommand.h"
#import "ETPPDebug.h"
#import "ETPPConstants.h"

typedef void(^PPResultBlock)(_Nullable id results);
typedef void(^PPFailedBlock)(NSError * _Nullable error);
typedef void(^PPContinueBlock)(_Nullable id attributes);

typedef NS_ENUM(NSInteger, MSRKeyInjectionStatus) {
    MSRKeyInjectionMSRConnectingDevice,
    MSRKeyInjectionResetMSRDeviceDisconnected,
    MSRKeyInjectionKSNReading,
    MSRKeyInjectionKSNReadingFailure,
    MSRKeyInjectionKSNReadingSuccess,
    MSRKeyInjectionKSNReadingDeviceDisconnected,    MSRKeyInjectionMUTReading,
    MSRKeyInjectionMUTReadingFailure,
    MSRKeyInjectionMUTReadingSuccess,
    MSRKeyInjectionMUTReadingDeviceDisconnected,
    MSRKeyInjectionKeyLoadCommandAPI,
    MSRKeyInjectionKeyLoadCommandAPISuccess,
    MSRKeyInjectionKeyLoadCommandAPIInternetConnectivityFailure,
    MSRKeyInjectionKeyLoadCommandAPIParseFailure,
    MSRKeyInjectionKeyLoadCommandAPIWebserviceFailure,
    MSRKeyInjectionCommandByKSNAPI,
    MSRKeyInjectionCommandByKSNAPISuccess,
    MSRKeyInjectionCommandByKSNAPIInternetConnectivityFailure,
    MSRKeyInjectionCommandByKSNAPIParseFailure,
    MSRKeyInjectionCommandByKSNAPIWebserviceFailure,
    MSRKeyInjectionWriteKeyLoadCommandToMSR,
    MSRKeyInjectionWriteKeyLoadCommandToMSRDeviceDisconnected,
    MSRKeyInjectionWriteKeyLoadCommandToMSRComplete,
    MSRKeyInjectionWriteKSNCommandToMSR,
    MSRKeyInjectionWriteKSNCommandToMSRDeviceDisconnected,
    MSRKeyInjectionWriteKSNCommandToMSRComplete,
    MSRKeyInjectionKSNTimeExpire,
    MSRKeyInjectionMUTTimeExpire,
    MSRKeyInjectionResetMSRTimeExpire,
    MSRKeyInjectionDefault
};

enum FWVersion
{
    FW9_11 = 1,
    FW9_X,
    FW9_29,
    FW10_x
};

@class CyEAConnection;

@protocol MSRInjectionDelegate;
@protocol FileDownloaderDelegate;

static NSInteger const ID_CFD_DEFAULT_SCROLL_SPEED = 2;
static NSInteger const ID_CFD_MAX_SCROLL_SPEED = 5;

// typedef void(^myCompletion)(BOOL);

@interface ETPPiDockControl : NSObject <NSURLSessionTaskDelegate,NSURLSessionDownloadDelegate> {
    // CyEAConnection *control;
    __weak id<MSRInjectionDelegate> msrInjectionDelegate;
    CyEAConnection *control;
    NSURLSessionDownloadTask *download;
    __weak id<FileDownloaderDelegate> _fileDownloaderDelegate;
}

@property (nonatomic, copy) void(^ _Nullable openCashDrawerCompletionHandler)(BOOL didOpen);
@property (nonatomic, weak) _Nullable id<MSRInjectionDelegate> msrInjectionDelegate;
@property (nonatomic, strong ) CyEAConnection * _Nullable control;

+ (_Nullable instancetype)hardwareInstance;

@property (assign) BOOL isPaperBinEmpty;
@property (assign) BOOL isCashDrawerOpen;
@property (assign) BOOL isConnectedToFirmware;
@property (assign) BOOL isBarcodeReaderOn;
@property (assign) BOOL isKSNCheck;
@property (assign) BOOL isResetMSRCheck;
@property (assign) BOOL isSecurityLevelCheck;
@property (nonatomic, assign,readonly) BOOL isConnected;
@property (assign) BOOL isCFDBacklightOn;
@property (assign) BOOL isChangeSecurityLevel;
@property (assign) BOOL isLogEnable;
@property (nonatomic, strong) NSString * _Nullable ksnString;
@property (nonatomic, strong) NSString * _Nullable mutString;
@property (nonatomic, strong) NSString * _Nullable customerCodeString;
@property (nonatomic, strong) NSString * _Nullable usernameString;
@property (nonatomic, strong) NSString * _Nullable passwordString;
@property (nonatomic, strong) NSString * _Nullable KSIDString;
@property (nonatomic, strong) NSString * _Nullable keyIdString;
@property (nonatomic, strong) NSString * _Nullable securityLevelString;
@property (nonatomic, strong) ETPPKeyInjectionAPICall * _Nullable keyInjectionAPICall;
@property (strong, nonatomic) ETPPCommand * _Nullable getKeyloadCommand;
@property (strong, nonatomic) ETPPCommand * _Nullable getCommandByKSN;
@property (strong, nonatomic) ETPPCommand * _Nullable getCommandByMUT;
@property (strong, nonatomic) NSMutableDictionary * _Nullable dictonaryGetKeyloadCommand;
@property (strong, nonatomic) NSMutableDictionary * _Nullable dictonaryGetCommandByKSN;
@property (strong, nonatomic) NSMutableDictionary * _Nullable dictonaryGetCommandByMUT;
@property (nonatomic, strong) NSURLSession * _Nullable backgroundSession;
@property (nonatomic, weak) _Nullable id <FileDownloaderDelegate> fileDownloaderDelegate;

- (void)establishFirmwareConnection;

//FW Reset Reason
- (void) getFWResetReason;
- (void) clearFWResetReason;


// Drawer
- (void)openDrawerWithCompletionHandler:(void (^_Nullable)(BOOL didOpen))completionHandler;
- (BOOL)checkCashDrawerStatus;

// Printer
- (void)checkPaperStatusWithCompletionHandler:(void (^ _Nullable )(BOOL paperBinIsEmpty))completionHandler;

- (void)printSync:(NSString * _Nullable )string;
- (void)printSync:(NSString * _Nullable )string withCompletionHandler:(void (^ _Nullable )(BOOL donePrinting))completionHandler;

- (void) printBarcode:(NSString * _Nullable )barcode withFormat:(ETPPBarcodeFormat)format completion:(void (^ _Nullable )(NSError * _Nullable ))completionHandler;
- (void)printBarcode:(NSString * _Nullable )barcode withFormat:(ETPPBarcodeFormat) format  withHeight: (NSUInteger) codeHeight withWidth:(ETPPBarcodeWidth) codeWidth withLeftMargin: (NSUInteger) leftMargin withHRILocation: (ETPPBarcodeHRILocation) hriLocation completion:(void (^ _Nullable )(NSError * _Nullable error)) completion;

- (void)printArabicString:(NSString * _Nullable )string withFontSize:(IDFontSize)fontSize withBold:(BOOL) isBold withUnderline:(BOOL) hasUnderline  completion:(void (^ _Nullable )(BOOL donePrinting))completionHandler;

- (void)resetPrinterFormats;
- (void)setPrinterFontSize:(IDFontSize)fontSize;
- (void)setPrinterUnderlined:(BOOL)isOn;
- (void)setPrinterBold:(BOOL)isOn;
- (void)setPrinterAlignment:(IDAlignment)alignment;

- (void)setPrinterLanguage:(IDLanguage)language;
- (IDLanguage)getPrinterLanguage;

- (void)cancelPrinting;

// Barcode
- (void) setBarcodeReaderOn:(BOOL)status;
- (void) toggleBarcodeReader; //HW 1.0 only
- (void) checkBarcodeReaderStatusWithCompletionHandler:(void (^ _Nullable )(BOOL isOn))completionHandler; // HW 1.0 Only
- (void) sendBCRCommand:(NSString * _Nullable )command; // HW 2.0 Only
- (void) sendBCRTriggerCommand:(BOOL)isOn; // HW 2.0 Only

// CFD
- (void)startDisplayCFDLineSync:(NSDictionary * _Nullable )dictionaryLines;
- (void)startDisplayCFDLineSync:(NSDictionary * _Nullable )dictionaryLines withComletionHandler:(void (^_Nullable)(BOOL)) completion;
- (void)setCustomerDisplayBacklightOn:(BOOL)state;
- (void)customerDisplayStopScroll;
- (void)customerDisplayClearText;
- (void)customerDisplayDecreaseLeftScrollSpeed;
- (void)customerDisplayIncreaseLeftScrollSpeed;
- (void)customerDisplayDecreaseRightScrollSpeed;
- (void)customerDisplayIncreaseRightScrollSpeed;

// Weight
- (void)getWeight;

- (void) getWeightWithScaleParser:(NSString * _Nullable (^ _Nullable)(uint8_t))parser;

// MSR
- (void)writeToMSRWithString:(NSString * _Nullable )string;
- (void)enableMSRConfig;
- (void)disableMSRConfig;
- (void)getKsnAndMut;
- (void)resetMSRCheckKey;
- (void)resetMSRCheckSL;
- (void)getSecurityLevelCheck;
- (void)changeSecurityLevel;
- (void)clearMSR;

- (void)readFWKSN;
- (void)readFWMUT;
- (void)resetFWMSR;
- (void)readFWSecurityLevel;

- (NSString * _Nullable )trimMSRResponse:(NSString * _Nullable )response;
- (NSString * _Nullable )trimKSN:(NSString * _Nullable )KSN;
- (BOOL)validateMSRResponse:(NSString * _Nullable )output;
- (void)calliOSKeyinjectionWithKSID:(NSString * _Nullable )KSID KeyID:(NSString * _Nullable )keyID CustomerCode:(NSString * _Nullable )customerCode Username:(NSString * _Nullable )username Password:(NSString * _Nullable )password SecurityLevel:(NSString * _Nullable )securityLevel;
-(void)KSNTimerAction:(NSTimer* _Nullable )timer;
-(void)MUTTimerAction:(NSTimer* _Nullable )timer;
-(void)resetMSRTimerAction:(NSTimer* _Nullable )timer;
-(void)getSecurityLevelTimerAction:(NSTimer* _Nullable )timer;
-(void)MSRConnectionTimerAction:(NSTimer* _Nullable )timer;
-(void)writeToMSRTimerAction:(NSTimer* _Nullable )timer;
-(void)writeCommandToMSR:(NSString * _Nullable )commandValue;

- (void) enableFWConnectCount;

- (void) disableFWConnectCount;


// Bootload
- (void)startBootloadWithFileName:(NSString * _Nullable )bootloadFileName;

// Info
- (void)getFWSerialNumber;
- (void)getFWVersion;
- (void)getHWVersion;

+ (enum FWVersion) getFWLoggedVersion;

+ (enum ETPPHWVersion) getHWLoggedVersion;

//SDK Version
-(NSString * _Nullable )getSDKVersion;

//Key Injection
- (void)magTekKeyInjectionAPICall:(NSDictionary * _Nullable )dictionaryCredentials;
- (void)magTekKeyInjectionSuccess:(NSString * _Nullable )response;
- (void)magTekKeyInjectionFailure:(NSError * _Nullable )error;

// Utility
+ (BOOL)appendData:(NSData * _Nullable )data count:(NSUInteger)count toString:(NSMutableString * _Nullable )string;

// Heartbeat, watchdog
- (void)setHeartbeat:(BOOL)enable;
- (void)enableWatchdog;

// Queue control
- (void)pauseAllTasks;
- (void)unpauseAllTasks;

- (void) cancelAllTasks;

// Debug methods
- (void)setDebugLevel:(ETPPDebugLevel)debugLevel;
- (ETPPDebugLevel)getDebugLevel;
- (void)setLoggerLocation:(ETPPLoggerLocation)loggerLocation;
- (void)clearLogFiles;
- (NSArray * _Nullable )logFilesPaths;


//Serial Speed

- (void) setSerialSpeed:(NSString * _Nullable )speed;

- (void) setWeightScaleCommand:(NSString * _Nullable )command;

- (void) sendDataToSerialPort:(NSData * _Nullable )data;

- (void) setDataSerialPortSpeed:(NSString * _Nullable )speed;

@end


@protocol FileDownloaderDelegate <NSObject>

@optional
- (void)fileDownloadProgress:(float)progress;
- (void)fileDownloadSuccess:(NSString * _Nullable )filePath;
- (void)fileDownloadFailure:(NSError * _Nullable )error;
@end

@protocol MSRInjectionDelegate<NSObject>

@optional
- (void)msrInjectionDelegateMethod:(MSRKeyInjectionStatus )msrKeyInjectionStatus messgae:(NSString * _Nullable )message;
- (void)securityLevelDelegateMethod:(NSString * _Nullable )securityLevelOutput;
@end
